<?php

return [
    'plugin' => [
        'name' => 'Places',
        'description' => 'Display your posts on Google Map and create a beautiful listing page'
    ],
	'settings' => [
        'map_title' => 'Map',
        'map_description' => 'Display your blog posts on Google map',
		'map_latitude' => 'Latitude',
		'map_latitude_description' => 'Set map center latitude',
		'map_longitude' => 'Longitude',
		'map_longitude_description' => 'Set map center longitude',
		'map_zoom' => 'Zoom',
		'map_zoom_description' => 'Set initial map zoom',
        'posts_pagination' => 'Page number',
        'posts_pagination_description' => 'This value is used to determine what page the user is on.',
        'posts_per_page' => 'Posts per page',
        'posts_per_page_validation' => 'Invalid format of the posts per page value',
        'posts_filter' => 'Category filter',
        'posts_filter_description' => 'Enter a category slug or URL parameter to filter the posts by. Leave empty to show all posts.',
        'posts_per_page' => 'Posts per page',
        'posts_per_page_validation' => 'Invalid format of the posts per page value',
        'posts_no_posts' => 'No posts message',
        'posts_no_posts_description' => 'Message to display in the post list in case if there are no posts.',
        'posts_no_posts_default' => 'No posts found',
        'posts_order' => 'Post order',
        'posts_order_description' => 'Attribute on which the posts should be ordered',
        'posts_post' => 'Post page',
        'posts_post_description' => 'Name of the blog post page file.',
        'group_links' => 'Links',        
    ]
];