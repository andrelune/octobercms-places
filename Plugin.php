<?php namespace Laravel42\Places;

use Event;
use Backend;
use System\Classes\PluginBase;
use RainLab\Blog\Models\Post; 
/**
 * Places Plugin Information File
 */
class Plugin extends PluginBase
{

	// required plugins
    public $require = ['Rainlab.Blog','Rainlab.Location'];    

    /**
     * Returns information about this plugin.
     *
     * @return array
     */
    public function pluginDetails()
    {
        return [
            'name'        => 'laravel42.places::lang.plugin.name',
            'description' => 'laravel42.places::lang.plugin.description',
            'author'      => 'Laravel42',
            'homepage'    => 'https://bitbucket.org/andrelune/octobercms-places'
        ];
    }

    /**
     * Register method, called when the plugin is first registered.
     *
     * @return void
     */
    public function register()
    {

    }

    /**
     * Boot method, called right before the request route.
     *
     * @return array
     */
    public function boot()
    {
    	Post::extend(function($model)
        {
        	// add foreign key
            $model->addFillable(['laravel42_places_address','laravel42_places_city','laravel42_places_zip','laravel42_places_country_code','laravel42_places_lat','laravel42_places_lng']);             
        });

        // Extend all backend form usage
        Event::listen('backend.form.extendFields', function($widget) {

            // Only for the Event controller
            if (!$widget->getController() instanceof \RainLab\Blog\Controllers\Posts) {
                return;
            }

            // Only for the Event model
            if (!$widget->model instanceof \RainLab\Blog\Models\Post) {
                return;
            }
            
            // Add extra fields
            $widget->addSecondaryTabFields([
                'laravel42_places_address' => [
                    'tab'  => 'Location',
                    'label'  => 'Address',
                    'span'  => 'full',
                    'type' => 'addressfinder',      
                    'fieldMap' => [
			            'latitude' => 'laravel42_places_lat',
			            'longitude' => 'laravel42_places_lng',
			            'city' => 'laravel42_places_city',
			            'zip' => 'laravel42_places_zip'
		           	]
                ],
                'laravel42_places_lat' => [
                    'tab'  => 'Location',
                    'label'  => 'Latitude',
                    'span'  => 'left',
                    'type' => 'text'          
                ],
                'laravel42_places_lng' => [
                    'tab'  => 'Location',
                    'label'  => 'Longitude',
                    'span'  => 'right',
                    'type' => 'text'          
                ], 
                'laravel42_places_city' => [
                    'tab'  => 'Location',
                    'label'  => 'City',
                    'span'  => 'left',
                    'type' => 'text'          
                ],
                'laravel42_places_zip' => [
                    'tab'  => 'Location',
                    'label'  => 'Zip',
                    'span'  => 'right',
                    'type' => 'text'          
                ]      
            ]);


        });
    }

    /**
     * Registers any front-end components implemented in this plugin.
     *
     * @return array
     */
    public function registerComponents()
    {
        return [
            'Laravel42\Places\Components\Map' => 'Places',
        ];
    }

    /**
     * Registers any back-end permissions used by this plugin.
     *
     * @return array
     */
    public function registerPermissions()
    {
        return []; // Remove this line to activate
        // return [
        //     'laravel42.places.some_permission' => [
        //         'tab' => 'Places',
        //         'label' => 'Some permission'
        //     ],
        // ];
    }
}
