<?php namespace Laravel42\Places\Components;

use BackendAuth;
use Cms\Classes\ComponentBase;
use Cms\Classes\Page;
use System\Classes\CombineAssets;

use RainLab\Blog\Models\Category;
use RainLab\Blog\Models\Post;
use RainLab\Location\Models\Setting;

use Redirect;
use Input;
use Lang;

class Map extends ComponentBase {
	/**
	 * A collection of posts to display
	 * @var Collection
	 */
	public $posts;

	/**
	 * A collection of posts to display within map
	 * @var Collection
	 */
	public $jsPosts;

	/**
     * Parameter to use for the page number
     *
     * @var string
     */
    public $pageParam;


	/**
     * If the post list should be ordered by another attribute
     *
     * @var string
     */
    public $sortOrder;

	/**
	 * If the post list should be filtered by a category, the model to use.
	 * @var Model
	 */
	public $category;

	/**
	 * Message to display when there are no posts.
	 * @var string
	 */
	public $noPostsMessage;

	/**
	 * Reference to the page name for linking to posts.
	 * @var string
	 */
	public $postPage;

	/**
	 * Center map latitude
	 * @var string
	 */
	public $lat;

	/**
	 * Center map longitude
	 * @var string
	 */
	public $lng;

	/**
	 * Map initial zoom
	 * @var string
	 */
	public $zoom;


	public function componentDetails() {
		return [
			'name'        => 'laravel42.places::lang.settings.map_title',
            'description' => 'laravel42.places::lang.settings.map_description'
		];
	}

	public function defineProperties() {
		return [
			'lat' => [
				'title' => 'laravel42.places::lang.settings.map_latitude',
				'description' => 'laravel42.places::lang.settings.map_latitude_description',
				'type' => 'string',
				'default' => '',
			],
			'lng' => [
				'title' => 'laravel42.places::lang.settings.map_longitude',
				'description' => 'laravel42.places::lang.settings.map_longitude_description',
				'type' => 'string',
				'default' => '',
			],
			'zoom' => [
				'title' => 'laravel42.places::lang.settings.map_zoom',
				'description' => 'laravel42.places::lang.settings.map_zoom_description',
				'type' => 'string',
				'default' => '5',
			],
			'pageNumber' => [
                'title'       => 'laravel42.places::lang.settings.posts_pagination',
                'description' => 'laravel42.places::lang.settings.posts_pagination_description',
                'type'        => 'string',
                'default'     => '{{ :page }}',
            ],
            'postsPerPage' => [
                'title'             => 'laravel42.places::lang.settings.posts_per_page',
                'type'              => 'string',
                'validationPattern' => '^[0-9]+$',
                'validationMessage' => 'rainlab.blog::lang.settings.posts_per_page_validation',
                'default'           => '10',
            ],
            'sortOrder' => [
                'title'       => 'laravel42.places::lang.settings.posts_order',
                'description' => 'laravel42.places::lang.settings.posts_order_description',
                'type'        => 'dropdown',                
                'default'     => 'published_at desc',
            ],
			'categoryFilter' => [
				'title' => 'laravel42.places::lang.settings.posts_filter',
				'description' => 'laravel42.places::lang.settings.posts_filter_description',
				'type' => 'string',
				'default' => '',
			],
			'noPostsMessage' => [
				'title' => 'laravel42.places::lang.settings.posts_no_posts',
				'description' => 'laravel42.places::lang.settings.posts_no_posts_description',
				'type' => 'string',
				'default' => Lang::get('laravel42.places::lang.settings.posts_no_posts_default'),
				'showExternalParam' => false,
			],
			'postPage' => [
				'title' => 'laravel42.places::lang.settings.posts_post',
				'description' => 'laravel42.places::lang.settings.posts_post_description',
				'type' => 'dropdown',
				'default' => 'blog/post',
				'group' => 'laravel42.places::lang.settings.group_links',
			]
		];
	}

	public function getSortOrderOptions() {
		return \RainLab\Blog\Models\Post::$allowedSortingOptions;
	}

	public function getPostPageOptions() {
		return Page::sortBy('baseFileName')->lists('baseFileName', 'baseFileName');
	}

	public function onRun() {
		
		$this->lat = $this->page['lat'] = $this->property('lat');
		$this->lng = $this->page['lng'] = $this->property('lng');
		$this->zoom = $this->page['zoom'] = $this->property('zoom');			

		$apiKey = Setting::get('google_maps_key');		
		
		$this->addCss('//cdnjs.cloudflare.com/ajax/libs/font-awesome/5.11.2/css/fontawesome.min.css');
		$this->addCss(
			CombineAssets::combine([				
				'/assets/css/style.css'	
			], $this->getLocalPath($this->assetPath))
		);

		$this->addJs('//maps.googleapis.com/maps/api/js?libraries=places&key=' . $apiKey);
		$this->addJs(
			CombineAssets::combine([
				'/assets/js/infobox.min.js',
				'/assets/js/markerclusterer.js',
				'/assets/js/map.js',
				'/assets/js/events.js',
			], $this->getLocalPath($this->assetPath))
		);
	}
	

	// Infobox Output
    protected function locationData($locationURL, $locationImg, $locationTitle, $locationAddress) {
	  return '<a href="' . $locationURL . '" class="listing-img-container">' .
	       '<div class="infoBox-close"><i class="fa fa-times"></i></div>' .
	       '<img src=" ' . $locationImg . '" alt="">' .
	       '<div class="listing-item-content">' .
	          '<h3>' . $locationTitle . '</h3>' .
	          '<span><i class="fa fa-map-marker"></i> ' . $locationAddress . '</span>' .
	       '</div>' .
	    '</a>';
	}

	protected function checkEditor() {
		$backendUser = BackendAuth::getUser();

		return $backendUser && $backendUser->hasAccess('rainlab.blog.access_posts');
	}

	protected function listJsPosts($posts) {		
		$jsPosts = array();

		foreach ($posts as $key => $post) {
			/*
			* JS dictionary
			*/			
			$featImg = $post->featured_images->count() ? $post->featured_images->first()->path : null;	
						
			$infoboxHtml = $this->locationData(
				join('/', [$post->url, $post->slug]), // url
				$featImg, // feat image
				$post->title,
				$post->address
			);
			$jsPosts[] = array(
				$infoboxHtml,
				$post->lat,
				$post->lng,
				$key, // index
				'<i class="fa fa-thumbtack"></i>', // cluster style
				$post->id // marker id
			);
		}

		return $jsPosts;
	}

	public function onDrawMap()
    {
    	/*
		* Page links
		*/
		$this->category = $this->page['category'] = $this->property('category');
		$this->postPage = $this->page['postPage'] = $this->property('postPage');
		$this->noPostsMessage = $this->page['noPostsMessage'] = $this->property('noPostsMessage');

    	/*
		* List all the posts, eager load their categories
		*/
		$isPublished = !$this->checkEditor();

		$posts = Post::isPublished()->whereNotNull('laravel42_places_lat')->whereNotNull('laravel42_places_lng')->with('categories')->listFrontEnd([
			'page'             => $this->property('pageNumber'),
            'sort'             => $this->property('sortOrder'),
            'perPage'          => $this->property('postsPerPage'),
			'category' 		   => $this->category,			
			'published'		   => $isPublished
		]);

		/*
         * Add a "url" helper attribute for linking to each post and category and fields alias
         */
        $posts->each(function($post) {
        	$post->lat = $post->laravel42_places_lat;
        	$post->lng = $post->laravel42_places_lng;
        	$post->address = $post->laravel42_places_address;
            $post->setUrl($this->postPage, $this->controller);
        });        
        

        $this->posts = $this->page['posts'] = $posts;
        $this->jsPosts = $this->page['jsPosts'] = $this->listJsPosts($this->posts);                
    }
}