var allMarkers = [];
var markerCluster;

(function($){
    "use strict";

    function addMarkers() {

      // Locations
      // ----------------------------------------------- //
      var ib = new InfoBox();

      var toggleVisibleListingItems = function() {
        // hide all
        $('.listing-item-container').each(function(){ $(this).parent().hide() });      

        var count = 0;
        for (var i = 0; i < allMarkers.length; i++){
          // get visible markers on the map
            if( map.getBounds().contains(allMarkers[i].getPosition()) ){
                // show visible items
                $('.listing-item-container[data-marker-id=' + allMarkers[i].args.marker_id + ']').parent().show();
                count++;
            }
        }
        // update results count
        $('.showing-results span').text(count);
      }

      // Remove old markers
      $('.map-marker-container').remove();
      if(markerCluster) markerCluster.clearMarkers();

      // Marker highlighting when hovering listing item
      $(document).on('mouseover','.listing-item-container', function(){        
        var listingAttr = $(this).data('marker-id');

        if(listingAttr !== undefined) {
          var listing_id = $(this).data('marker-id');
          var marker_div = false; 
          for (var i = 0; i < allMarkers.length; i++) {
            if (allMarkers[i].args.marker_id == listing_id) 
              marker_div = allMarkers[i].div;
          }

          if ( marker_div ) {
            $(marker_div).addClass('clicked');

            $(this).on('mouseout', function(){
                if ($(marker_div).is(":not(.infoBox-opened)")) {
                   $(marker_div).removeClass('clicked');
                }
             });
          }
        }

      });


      // Infobox
      // ----------------------------------------------- //
      var boxText = document.createElement("div");
      boxText.className = 'map-box'

      var currentInfobox;

      var boxOptions = {
              content: boxText,
              disableAutoPan: false,
              alignBottom : true,
              maxWidth: 0,
              pixelOffset: new google.maps.Size(-134, -55),
              zIndex: null,
              boxStyle: {
                width: "270px"
              },
              closeBoxMargin: "0",
              closeBoxURL: "",
              infoBoxClearance: new google.maps.Size(25, 25),
              isHidden: false,
              pane: "floatPane",
              enableEventPropagation: false,
      };


      var overlay, i;      
      var markerIco;
      var markerId;
      for (i = 0; i < locations.length; i++) {

        markerIco = locations[i][4];
        markerId = locations[i][5];

        var overlaypositions = new google.maps.LatLng(locations[i][1], locations[i][2]),

        overlay = new CustomMarker(
         overlaypositions,
          map,
          {
            marker_id: markerId
          },
          markerIco
        );

        allMarkers.push(overlay);

        google.maps.event.addDomListener(overlay, 'click', (function(overlay, i) {

        return function() {
             ib.setOptions(boxOptions);
             boxText.innerHTML = locations[i][0];
             ib.close();
             ib.open(map, overlay);
             currentInfobox = locations[i][3];

            google.maps.event.addListener(ib,'domready',function(){
              $('.infoBox-close').click(function(e) {
                  e.preventDefault();
                  ib.close();
                  $('.map-marker-container').removeClass('clicked infoBox-opened');
              });

            });

          }
        })(overlay, i));

      }


      // Marker Clusterer Init
      // ----------------------------------------------- //
      var options = {
          imagePath: '/plugins/laravel42/places/assets/images/m',
          minimumClusterSize : 2
      };

      markerCluster = new MarkerClusterer(map, allMarkers, options);            
    }

    setTimeout(function(){
      addMarkers();
    }, 200);


    // ---------------- Main Map / End ---------------- //
  
    // Custom Map Marker
    // ----------------------------------------------- //

    function CustomMarker(latlng, map, args, markerIco) {
      this.latlng = latlng;
      this.args = args;
      this.markerIco = markerIco;  
      this.setMap(map);      
    }

    CustomMarker.prototype = new google.maps.OverlayView();

    CustomMarker.prototype.draw = function() {

      var self = this;

      var div = this.div;

      if (!div) {

        div = this.div = document.createElement('div');
        div.className = 'map-marker-container';

        div.innerHTML = '<div class="marker-container">'+
                            '<div class="marker-card">'+
                               '<div class="front face">' + self.markerIco + '</div>'+
                               '<div class="back face">' + self.markerIco + '</div>'+
                               '<div class="marker-arrow"></div>'+
                            '</div>'+
                          '</div>'


        // Clicked marker highlight
        google.maps.event.addDomListener(div, "click", function(event) {
            $('.map-marker-container').removeClass('clicked infoBox-opened');
            google.maps.event.trigger(self, "click");
            $(this).addClass('clicked infoBox-opened');
        });


        if (typeof(self.args.marker_id) !== 'undefined') {
          div.dataset.marker_id = self.args.marker_id;
        }

        var panes = this.getPanes();
        panes.overlayImage.appendChild(div);
      }

      var point = this.getProjection().fromLatLngToDivPixel(this.latlng);

      if (point) {
        div.style.left = (point.x) + 'px';
        div.style.top = (point.y) + 'px';
      }
    };

    CustomMarker.prototype.remove = function() {
      if (this.div && this.div.parentNode) {
        this.div.parentNode.removeChild(this.div);
        this.div = null; $(this).removeClass('clicked');
      }
    };

    CustomMarker.prototype.getPosition = function() { return this.latlng; };

    CustomMarker.prototype.getDraggable = function() { return false; };

    // -------------- Custom Map Marker / End -------------- //
})(this.jQuery);
