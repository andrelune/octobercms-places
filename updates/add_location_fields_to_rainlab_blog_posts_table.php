<?php namespace Laravel42\Places\Updates;

use October\Rain\Database\Updates\Migration;
use Schema;

class AddNewFieldToProeventsTable extends Migration
{
    public function up()
    {
        Schema::table('rainlab_blog_posts', function($table)
        {
            $table->string('laravel42_places_address', 191)->nullable();
            $table->string('laravel42_places_city', 191)->nullable();
            $table->string('laravel42_places_zip', 20)->nullable();
            $table->string('laravel42_places_country_code', 4)->nullable();
            $table->decimal('laravel42_places_lat', 16, 14)->nullable();
            $table->decimal('laravel42_places_lng', 17, 14)->nullable();     
        });
    }

    public function down()
    {
        Schema::table('rainlab_blog_posts', function($table)
        {
            $table->dropColumn(['laravel42_places_address','laravel42_places_city','laravel42_places_zip','laravel42_places_country_code','laravel42_places_lat','laravel42_places_lng']);
        });
    }
}